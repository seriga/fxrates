package com.seriga.fxrates

import android.app.Application
import android.content.Context
import com.seriga.fxrates.di.app.AppComponent
import com.seriga.fxrates.di.app.AppModule
import com.seriga.fxrates.di.app.DaggerAppComponent

/**
 * Created by seriga on 28.02.2017.
 */

class RatesApplication : Application() {

    lateinit var appComponent: AppComponent
        private set

    companion object {

        fun get(context: Context) = context.applicationContext as RatesApplication
    }

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

}
