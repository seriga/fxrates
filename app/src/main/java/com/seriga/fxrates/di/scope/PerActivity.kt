package com.seriga.fxrates.di.scope

import javax.inject.Scope

/**
 * Created by seriga on 28.02.2017.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity
