package com.seriga.fxrates.di.app

import android.content.Context

import com.seriga.fxrates.data.RatesParser
import com.seriga.fxrates.data.Repository
import com.seriga.fxrates.di.scope.PerApp

import dagger.Component

/**
 * Created by seriga on 28.02.2017.
 */

@PerApp
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    val context: Context

    val repository: Repository

    val ratesParser: RatesParser

}
