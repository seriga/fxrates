package com.seriga.fxrates.di.app

import android.app.Application
import android.content.Context

import com.seriga.fxrates.data.FxRatesApi
import com.seriga.fxrates.data.RatesParser
import com.seriga.fxrates.data.Repository
import com.seriga.fxrates.di.scope.PerApp

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

/**
 * Created by seriga on 28.02.2017.
 */

const val BASE_URL = "http://www.ecb.europa.eu"

@Module
class AppModule(private val application: Application) {

    @PerApp
    @Provides
    internal fun provideContext(): Context = application

    @PerApp
    @Provides
    @BaseUrl
    internal fun provideBaseUrl() = BASE_URL

    @PerApp
    @Provides
    internal fun provideCallAdapterFactory() = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

    @PerApp
    @Provides
    internal fun provideRetrofit(@BaseUrl baseUrl: String, callAdapterFactory: RxJava2CallAdapterFactory) = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(callAdapterFactory)
            .build()

    @PerApp
    @Provides
    internal fun provideFxRatesApi(retrofit: Retrofit) = retrofit.create(FxRatesApi::class.java)

    @PerApp
    @Provides
    internal fun provideRepository(fxRatesApi: FxRatesApi) = Repository(fxRatesApi)

    @PerApp
    @Provides
    internal fun provideRatesParser() = RatesParser()

}
