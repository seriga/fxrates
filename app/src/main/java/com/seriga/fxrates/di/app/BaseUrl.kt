package com.seriga.fxrates.di.app

import javax.inject.Qualifier

/**
 * Created by seriga on 28.02.2017.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseUrl
