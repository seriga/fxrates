package com.seriga.fxrates.util

import android.support.annotation.IntegerRes
import android.support.v7.widget.Toolbar
import android.util.TypedValue
import android.view.View
import com.seriga.fxrates.R

/**
 * Created by seriga on 01.03.2017.
 */
inline fun <reified T : View> viewOrThrow(view: T?) = view ?: throw IllegalStateException("${T::class.java} is null")

fun Toolbar.actionBarSize(): Int {
    var result = 0
    val typedValue = TypedValue()
    if (context.theme.resolveAttribute(R.attr.actionBarSize, typedValue, true)) {
        result = TypedValue.complexToDimensionPixelSize(typedValue.data, resources.displayMetrics)
    }
    return result
}

fun View.integer(@IntegerRes resId: Int) = resources.getInteger(resId)