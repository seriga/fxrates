package com.seriga.fxrates.view

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View
import android.view.ViewManager
import org.jetbrains.anko.custom.ankoView

/**
 * Created by seriga on 04.03.2017.
 */
class ViewPagerWrapContent : ViewPager {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (childCount > 0) {
            val child = getChildAt(0)
            child.measure(widthMeasureSpec, heightMeasureSpec)
            val width = View.resolveSize(child.measuredWidth, widthMeasureSpec)
            val height = View.resolveSize(child.measuredHeight, heightMeasureSpec)
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.getMode(widthMeasureSpec)),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.getMode(heightMeasureSpec)))
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

}

@Suppress("NOTHING_TO_INLINE")
inline fun ViewManager.viewPagerWrapContent(theme: Int = 0) = viewPagerWrapContent(theme) { }

inline fun ViewManager.viewPagerWrapContent(theme: Int = 0, init: ViewPagerWrapContent.() -> Unit) = ankoView(::ViewPagerWrapContent, theme, init)