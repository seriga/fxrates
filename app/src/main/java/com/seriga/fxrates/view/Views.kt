package com.seriga.fxrates.view

import android.view.ViewManager
import com.seriga.fxrates.R
import com.seriga.fxrates.util.actionBarSize
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.matchParent

/**
 * Created by seriga on 12.03.2017.
 */
fun ViewManager.toolbarCommon() = toolbar(R.style.AppTheme_AppBarOverlay) {
    backgroundResource = R.color.colorPrimary
    lparams(width = matchParent, height = actionBarSize())
}