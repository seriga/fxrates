package com.seriga.fxrates.base.presenter

import android.content.Context
import android.support.v4.content.Loader

/**
 * Created by seriga on 28.02.2017.
 */

class PresenterLoader<P : BasePresenter<*>>(context: Context, var presenter: P) : Loader<P>(context) {

    override fun onReset() {
        presenter.onDestroyed()
    }

}
