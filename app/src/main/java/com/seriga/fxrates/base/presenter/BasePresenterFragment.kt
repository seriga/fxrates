package com.seriga.fxrates.base.presenter

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.view.View
import com.seriga.fxrates.base.BaseFragment

/**
 * Created by seriga on 28.02.2017.
 */

abstract class BasePresenterFragment<P : BasePresenter<V>, V : BaseView> : BaseFragment(), LoaderManager.LoaderCallbacks<P> {

    protected lateinit var presenter: P

    protected abstract fun createPresenter(): P

    @get:IdRes
    protected abstract val presenterLoaderId: Int

    protected abstract val presenterView: V

    protected open fun inject() {}

    protected open fun initViews() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        initPresenter()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        presenter.attachView(presenterView)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun initPresenter() {
        val loaderManager = loaderManager
        val loader = loaderManager.getLoader<Any>(presenterLoaderId)
        if (loader == null) {
            presenter = createPresenter()
            loaderManager.initLoader(presenterLoaderId, null, this)
        } else {
            @Suppress("UNCHECKED_CAST")
            presenter = (loader as PresenterLoader<P>).presenter
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<P> {
        return PresenterLoader(context, presenter)
    }

    override fun onLoadFinished(loader: Loader<P>, data: P) {
//        ignored
    }

    override fun onLoaderReset(loader: Loader<P>) {
//        ignored
    }

}
