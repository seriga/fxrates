package com.seriga.fxrates.base

import android.support.annotation.UiThread
import android.support.v4.app.Fragment

/**
 * Created by seriga on 28.02.2017.
 */

@UiThread
open class BaseFragment : Fragment()
