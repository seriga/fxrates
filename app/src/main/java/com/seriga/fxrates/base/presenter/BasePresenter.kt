package com.seriga.fxrates.base.presenter

/**
 * Created by seriga on 28.02.2017.
 */

interface BasePresenter<in V : BaseView> {

    fun attachView(view: V)

    fun detachView()

    fun onDestroyed()

}
