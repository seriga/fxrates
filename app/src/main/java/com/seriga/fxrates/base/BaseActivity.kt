package com.seriga.fxrates.base

import android.support.annotation.UiThread
import android.support.v7.app.AppCompatActivity

/**
 * Created by seriga on 28.02.2017.
 */

@UiThread
abstract class BaseActivity : AppCompatActivity()
