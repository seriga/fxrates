package com.seriga.fxrates.rates.di

import com.seriga.fxrates.di.app.AppComponent
import com.seriga.fxrates.di.scope.PerActivity
import com.seriga.fxrates.rates.RatesFragment
import com.seriga.fxrates.rates.RatesPresenter
import dagger.Component

/**
 * Created by seriga on 28.02.2017.
 */

@PerActivity
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(RatesModule::class))
interface RatesComponent {

    fun inject(ratesFragment: RatesFragment)

    val ratesPresenter: RatesPresenter

}
