package com.seriga.fxrates.rates.adapter

import android.os.Parcel
import android.os.Parcelable
import android.support.v4.util.Pools
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.seriga.fxrates.data.Currency

/**
 * Created by seriga on 04.03.2017.
 */

private const val POOL_SIZE = 5

class RatesPagerAdapter(val pagerId: Int) : PagerAdapter() {

    var items: List<Currency>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var listener: OnAmountChangedListener? = null
    var currentPosition = 0
    private val views = mutableMapOf<Int, View>()
    private val viewPool = Pools.SimplePool<View>(POOL_SIZE)
    private var pendingState: SavedState? = null

    override fun isViewFromObject(view: View?, `object`: Any?) = view == `object`

    override fun getCount() = items?.size ?: 0

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = getView(container)
        val holder = getHolderFromView(view)
        holder.bind(items!![position])
        applyPendingState(holder, position)
        views.put(position, view)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any?) {
        val view = `object` as View
        viewPool.release(view)
        views.remove(position)
        container.removeView(view)
    }

    override fun notifyDataSetChanged() {
        super.notifyDataSetChanged()
        val localItems = items
        if (localItems != null && localItems.isNotEmpty()) {
            for ((position, view) in views) {
                val holder = getHolderFromView(view)
                holder.bind(localItems[position])
            }
        }
    }

    override fun saveState(): Parcelable? {
        var result: Parcelable? = null
        val amount = getAmount(currentPosition)
        if (amount > 0) {
            result = SavedState(currentPosition, amount)
        }
        return result
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        pendingState = state as? SavedState
    }

    fun getAmount(position: Int) = getHolderByPosition(position)?.getAmount() ?: 0.0

    fun setAmount(position: Int, amount: Double) {
        getHolderByPosition(position)?.setAmount(amount)
    }

    fun clearAmount(position: Int) {
        getHolderByPosition(position)?.clearAmount()
    }

    private fun getView(container: ViewGroup): View {
        var view = viewPool.acquire()
        if (view == null) {
            view = RateHolder(container.context, pagerId, listener).view
        }
        return view
    }

    private fun getHolderByPosition(position: Int) = views[position]?.tag as? RateHolder

    private fun getHolderFromView(view: View) = view.tag as RateHolder

    private fun applyPendingState(holder: RateHolder, position: Int) {
        val localPendingState = pendingState
        if (localPendingState != null && localPendingState.currentPosition == position) {
            holder.setAmount(localPendingState.currentAmount)
            pendingState = null
        }
    }

    class SavedState(val currentPosition: Int, val currentAmount: Double) : Parcelable {

        constructor(source: Parcel): this(source.readInt(), source.readDouble())

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeInt(currentPosition)
            dest.writeDouble(currentAmount)
        }

        override fun describeContents() = 0

        companion object {

            @Suppress("unused", "RedundantModalityModifier")
            @JvmField
            final val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {

                override fun createFromParcel(source: Parcel): SavedState {
                    return SavedState(source)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    interface OnAmountChangedListener {
        fun onAmountChanged(pagerId:Int, amount: Double)
    }

}