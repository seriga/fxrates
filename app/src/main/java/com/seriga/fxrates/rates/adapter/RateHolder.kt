package com.seriga.fxrates.rates.adapter

import android.content.Context
import android.text.InputFilter
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.seriga.fxrates.R
import com.seriga.fxrates.data.Currency
import com.seriga.fxrates.util.integer
import org.jetbrains.anko.*

/**
 * Created by seriga on 12.03.2017.
 */
internal class RateHolder(context: Context, val pagerId: Int, val listener: RatesPagerAdapter.OnAmountChangedListener?) {

    val view: View
    lateinit var titleView: TextView
    lateinit var amountView: EditText
    internal var userInput = true

    init {
        view = RateLayout().createView(AnkoContext.create(context, this))
        view.tag = this
    }

    fun bind(item: Currency) {
        val title = item.type.value
        if (!TextUtils.equals(title, titleView.text)) {
            titleView.text = title
        }
    }

    fun getAmount() = amountView.text.toString().toDoubleOrNull() ?: 0.0

    fun setAmount(amount: Double) {
        setAmountInternal(amount.toString())
    }

    fun clearAmount() {
        setAmountInternal(null)
    }

    private fun setAmountInternal(amount: String?) {
        userInput = false
        amountView.setText(amount)
    }

}

internal class RateLayout : AnkoComponent<RateHolder> {

    override fun createView(ui: AnkoContext<RateHolder>) = with(ui) {
        relativeLayout {
            padding = dimen(R.dimen.item_rates_padding)
            lparams(width = matchParent, height = wrapContent)

            ui.owner.titleView = textView {
                id = R.id.rate_layout_title
                textSizeDimen = R.dimen.item_rates_title_text_size
            }.lparams {
                width = matchParent
                height = wrapContent
                leftOf(R.id.rate_layout_amount)
            }

            ui.owner.amountView = editText {
                id = R.id.rate_layout_amount
                background = null
                inputType = EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_FLAG_DECIMAL
                ellipsize = TextUtils.TruncateAt.END
                maxLines = 1
                filters = arrayOf(InputFilter.LengthFilter(integer(R.integer.item_rates_amount_max_length)))
                hintResource = R.string.item_rates_amount_hint
                textSizeDimen = R.dimen.item_rates_amount_text_size
                textChangedListener {
                    afterTextChanged {
                        // TODO debounce
                        if (ui.owner.userInput) {
                            ui.owner.listener?.onAmountChanged(ui.owner.pagerId, it.toString().toDoubleOrNull() ?: 0.0)
                        } else {
                            ui.owner.userInput = true
                        }
                    }
                }
            }.lparams {
                width = wrapContent
                height = wrapContent
                alignParentEnd()
                alignParentRight()
                centerVertically()
            }
        }
    }

}