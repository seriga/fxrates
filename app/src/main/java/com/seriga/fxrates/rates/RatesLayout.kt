package com.seriga.fxrates.rates

import com.seriga.fxrates.R
import com.seriga.fxrates.view.toolbarCommon
import com.seriga.fxrates.view.viewPagerWrapContent
import org.jetbrains.anko.*

/**
 * Created by seriga on 11.03.2017.
 */

class RatesLayout : AnkoComponent<RatesFragment> {

    override fun createView(ui: AnkoContext<RatesFragment>) = with(ui) {
        verticalLayout {
            ui.owner.apply {
                myToolbar = toolbarCommon()

                myFirstRatesPager = viewPagerWrapContent {
                    id = R.id.rates_layout_first_pager
                }.lparams(width = matchParent, height = wrapContent)

                mySecondRatesPager = viewPagerWrapContent {
                    id = R.id.rates_layout_second_pager
                }.lparams(width = matchParent, height = wrapContent)
            }
        }
    }

    fun unbind(owner: RatesFragment) {
        owner.apply {
            myToolbar = null
            myFirstRatesPager = null
            mySecondRatesPager = null
        }
    }

}