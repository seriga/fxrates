package com.seriga.fxrates.rates

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.seriga.fxrates.R
import com.seriga.fxrates.RatesApplication
import com.seriga.fxrates.base.presenter.BasePresenterFragment
import com.seriga.fxrates.data.Currency
import com.seriga.fxrates.rates.adapter.RatesPagerAdapter
import com.seriga.fxrates.rates.di.*
import com.seriga.fxrates.util.viewOrThrow
import org.jetbrains.anko.AnkoContext
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by seriga on 28.02.2017.
 */

class RatesFragment : BasePresenterFragment<RatesContract.Presenter, RatesContract.View>(), RatesContract.View,
        RatesPagerAdapter.OnAmountChangedListener {

    @field:[Inject Named(FIRST_ADAPTER)]
    internal lateinit var ratesAdapterFirst: RatesPagerAdapter

    @field:[Inject Named(SECOND_ADAPTER)]
    internal lateinit var ratesAdapterSecond: RatesPagerAdapter

    internal var myToolbar: Toolbar? = null
    private val toolbar: Toolbar
        get() = viewOrThrow(myToolbar)

    internal var myFirstRatesPager: ViewPager? = null
    private val firstRatesPager
        get() = viewOrThrow(myFirstRatesPager)

    internal var mySecondRatesPager: ViewPager? = null
    private val secondRatesPager
        get() = viewOrThrow(mySecondRatesPager)

    private val layout = RatesLayout()
    private lateinit var ratesComponent: RatesComponent

    companion object {

        fun newInstance() = RatesFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ratesAdapterFirst.listener = this
        ratesAdapterSecond.listener = this
    }

    override fun inject() {
        super.inject()
        ratesComponent = DaggerRatesComponent.builder()
                .appComponent(RatesApplication.get(context).appComponent)
                .ratesModule(RatesModule())
                .build()
        ratesComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layout.createView(AnkoContext.create(context, this))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        layout.unbind(this)
    }

    override fun initViews() {
        super.initViews()
        initToolbar()
        initViewPager(firstRatesPager,
                ratesAdapterFirst,
                secondRatesPager,
                ratesAdapterSecond)
        initViewPager(secondRatesPager,
                ratesAdapterSecond,
                firstRatesPager,
                ratesAdapterFirst)
    }

    override fun onStart() {
        super.onStart()
        presenter.startPolling()
    }

    override fun onStop() {
        super.onStop()
        presenter.stopPolling()
    }

    override fun onAmountChanged(pagerId:Int, amount: Double) {
        when (pagerId) {
            RATES_PAGER_FIRST_ID -> {
                presenter.onAmountChanged(
                        pagerId,
                        firstRatesPager.currentItem,
                        secondRatesPager.currentItem,
                        amount)
            }
            RATES_PAGER_SECOND_ID -> {
                presenter.onAmountChanged(
                        pagerId,
                        secondRatesPager.currentItem,
                        firstRatesPager.currentItem,
                        amount)
            }
        }
    }

    private fun initToolbar() {
        val activity = activity as? AppCompatActivity
        activity?.setSupportActionBar(toolbar)
    }

    private fun initViewPager(viewPager: ViewPager,
                              adapter: RatesPagerAdapter,
                              dependentPager: ViewPager,
                              dependentPagerAdapter: RatesPagerAdapter) {
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {

            override fun onPageSelected(position: Int) {
                adapter.currentPosition = position
                presenter.onPageSelected(adapter.pagerId,
                        dependentPager.currentItem,
                        viewPager.currentItem,
                        dependentPagerAdapter.getAmount(dependentPager.currentItem))
            }
        })
    }

    private fun updateAmount(adapter: RatesPagerAdapter, position:Int, amount: Double) {
        if (amount > 0) {
            adapter.setAmount(position, amount)
        } else {
            adapter.clearAmount(position)
        }
    }

    override fun setData(items: List<Currency>) {
        ratesAdapterFirst.items = items
        ratesAdapterSecond.items = items
    }

    override fun updateAmount(activePager:Int, amount: Double) {
        when (activePager) {
            RATES_PAGER_FIRST_ID -> updateAmount(ratesAdapterSecond, secondRatesPager.currentItem, amount)
            RATES_PAGER_SECOND_ID -> updateAmount(ratesAdapterFirst, firstRatesPager.currentItem, amount)
        }
    }

    override fun clearAmount() {
        ratesAdapterFirst.clearAmount(firstRatesPager.currentItem)
        ratesAdapterSecond.clearAmount(secondRatesPager.currentItem)
    }

    //region Presenter fragment
    override fun createPresenter() = ratesComponent.ratesPresenter

    override val presenterLoaderId = R.id.presenter_loader_rates

    override val presenterView = this
    //endregion

}
