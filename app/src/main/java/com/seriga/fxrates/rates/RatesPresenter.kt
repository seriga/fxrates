package com.seriga.fxrates.rates

import com.seriga.fxrates.data.Currency
import com.seriga.fxrates.data.RatesParser
import com.seriga.fxrates.data.Repository
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by seriga on 28.02.2017.
 */

class RatesPresenter(private val repository: Repository,
                     private val ratesParser: RatesParser,
                     private val delayTime: Long) : RatesContract.Presenter {

    private var view: RatesContract.View? = null

    private var pollingDisposable: Disposable? = null
    private var data: List<Currency>? = null
    private var activePager: Int = NO_ID

    override fun attachView(view: RatesContract.View) {
        this.view = view
        val localData = data
        if (localData != null) {
            view.setData(localData)
        }
    }

    override fun detachView() {
        view = null
    }

    override fun onDestroyed() {
        view = null
        stopPolling()
    }

    override fun startPolling() {
        if (isPolling()) {
            return
        }
        pollingDisposable = repository.fxRates()
                .subscribeOn(Schedulers.io())
                .repeatWhen { it.compose(delayTransformer()) }
                .retryWhen { it.compose(delayTransformer()) }
                .observeOn(Schedulers.computation())
                .map { ratesParser.parseRates(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    data = it
                    view?.setData(it)
                }
    }

    override fun stopPolling() {
        if (isPolling()) {
            pollingDisposable?.dispose()
            pollingDisposable = null
        }
    }

    override fun onAmountChanged(pagerId:Int, fromCurrencyPosition: Int, toCurrencyPosition: Int, amount: Double) {
        activePager = pagerId
        updateAmount(fromCurrencyPosition, toCurrencyPosition, amount)
    }

    override fun onPageSelected(pagerId:Int, fromCurrencyPosition: Int, toCurrencyPosition: Int, amount: Double) {
        if (activePager == pagerId) {
            view?.clearAmount()
        } else {
            updateAmount(fromCurrencyPosition, toCurrencyPosition, amount)
        }
    }

    private fun isPolling(): Boolean {
        val disposable = pollingDisposable
        return disposable != null && !disposable.isDisposed
    }

    private fun <T> delayTransformer() = ObservableTransformer<T, T> { it.delay(delayTime, TimeUnit.SECONDS) }

    private fun updateAmount(fromCurrencyPosition: Int, toCurrencyPosition: Int, amount: Double) {
        val localData = data ?: return
        val fromCurrency = localData[fromCurrencyPosition]
        val toCurrency = localData[toCurrencyPosition]
        val result = amount * (toCurrency.rate / fromCurrency.rate)
        view?.updateAmount(activePager, result)
    }

}
