package com.seriga.fxrates.rates

import com.seriga.fxrates.base.presenter.BasePresenter
import com.seriga.fxrates.base.presenter.BaseView
import com.seriga.fxrates.data.Currency

/**
 * Created by seriga on 28.02.2017.
 */

const val NO_ID = -1
const val RATES_PAGER_FIRST_ID = 0
const val RATES_PAGER_SECOND_ID = 1

interface RatesContract {

    interface View : BaseView {
        fun setData(items: List<Currency>)
        fun updateAmount(activePager:Int, amount: Double)
        fun clearAmount()
    }

    interface Presenter : BasePresenter<View> {
        fun startPolling()
        fun stopPolling()
        fun onAmountChanged(pagerId:Int, fromCurrencyPosition: Int, toCurrencyPosition: Int, amount:Double)
        fun onPageSelected(pagerId:Int, fromCurrencyPosition: Int, toCurrencyPosition: Int, amount:Double)
    }

}