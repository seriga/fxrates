package com.seriga.fxrates.rates.di

import com.seriga.fxrates.data.RatesParser
import com.seriga.fxrates.data.Repository
import com.seriga.fxrates.di.scope.PerActivity
import com.seriga.fxrates.rates.RATES_PAGER_FIRST_ID
import com.seriga.fxrates.rates.RATES_PAGER_SECOND_ID
import com.seriga.fxrates.rates.RatesPresenter
import com.seriga.fxrates.rates.adapter.RatesPagerAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Created by seriga on 28.02.2017.
 */

const val INTERVAL_POLLING = 30L
const val FIRST_ADAPTER = "FIRST_ADAPTER"
const val SECOND_ADAPTER = "SECOND_ADAPTER"

@Module
class RatesModule {

    @PerActivity
    @Provides
    internal fun provideRatesPresenter(repository: Repository, parser: RatesParser) = RatesPresenter(repository, parser, INTERVAL_POLLING)

    @PerActivity
    @Named(FIRST_ADAPTER)
    @Provides
    internal fun provideRatesFromAdapter() = RatesPagerAdapter(RATES_PAGER_FIRST_ID)

    @PerActivity
    @Named(SECOND_ADAPTER)
    @Provides
    internal fun provideRatesToAdapter() = RatesPagerAdapter(RATES_PAGER_SECOND_ID)

}
