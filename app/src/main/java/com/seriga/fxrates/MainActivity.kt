package com.seriga.fxrates

import android.os.Bundle
import com.seriga.fxrates.base.BaseActivity
import com.seriga.fxrates.rates.RatesFragment
import org.jetbrains.anko.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainLayout().setContentView(this)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.main_activity_content,
                            RatesFragment.newInstance(),
                            RatesFragment::class.java.simpleName)
                    .commit()
        }
    }

    private class MainLayout : AnkoComponent<MainActivity> {

        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
            frameLayout {
                id = R.id.main_activity_content
                lparams(width = matchParent, height = matchParent)
            }
        }
    }

}