package com.seriga.fxrates.data

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET

/**
 * Created by seriga on 24.02.2017.
 */

interface FxRatesApi {

    @GET("/stats/eurofxref/eurofxref-daily.xml")
    fun fxRates(): Observable<ResponseBody>

}