package com.seriga.fxrates.data

import android.support.annotation.WorkerThread
import io.reactivex.Observable
import okhttp3.ResponseBody

/**
 * Created by seriga on 24.02.2017.
 */

@WorkerThread
class Repository(private val fxRatesApi: FxRatesApi) {

    fun fxRates(): Observable<ResponseBody> = fxRatesApi.fxRates()

}
