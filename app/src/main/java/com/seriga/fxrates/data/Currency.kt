package com.seriga.fxrates.data

/**
 * Created by seriga on 24.02.2017.
 */

data class Currency(val type: Currency.Type, val rate: Double) {

    enum class Type(val value: String) {
        GBP("GBP"),
        EUR("EUR"),
        USD("USD");

        companion object {

            fun fromValue(value: String?) = values().firstOrNull { it.value == value }
        }
    }

}
