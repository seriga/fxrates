package com.seriga.fxrates.data

import android.support.annotation.WorkerThread
import android.util.Xml
import okhttp3.ResponseBody
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException

/**
 * Created by seriga on 24.02.2017.
 */

const val TAG_NAME_CURRENCY = "Cube"
const val ATTRIBUTE_NAME_CURRENCY = "currency"
const val ATTRIBUTE_NAME_RATE = "rate"

@WorkerThread
class RatesParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parseRates(responseBody: ResponseBody): List<Currency> {
        val result: List<Currency>? =
                responseBody.use { responseBody ->
                    val parser = Xml.newPullParser()
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                    parser.setInput(responseBody.charStream())
                    parser.nextTag()
                    readBody(parser)
                }
        return result ?: emptyList()
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readBody(parser: XmlPullParser): List<Currency>? {
        var result: List<Currency>? = null
        if (goToTag(parser, TAG_NAME_CURRENCY)) {
            result = readSubBody(parser)
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun goToTag(parser: XmlPullParser, tag: String): Boolean {
        var result = false
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == tag) {
                result = true
                break
            } else {
                skip(parser)
            }
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readSubBody(parser: XmlPullParser): List<Currency>? {
        parser.require(XmlPullParser.START_TAG, null, TAG_NAME_CURRENCY)
        var result: List<Currency>? = null
        if (goToTag(parser, TAG_NAME_CURRENCY)) {
            result = readCurrencies(parser)
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readCurrencies(parser: XmlPullParser): List<Currency> {
        parser.require(XmlPullParser.START_TAG, null, TAG_NAME_CURRENCY)
        val result = mutableListOf<Currency>()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == TAG_NAME_CURRENCY) {
                val currency = readCube(parser)
                if (currency != null) {
                    result.add(currency)
                }
            } else {
                skip(parser)
            }
        }
        return result
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readCube(parser: XmlPullParser): Currency? {
        var result: Currency? = null
        parser.require(XmlPullParser.START_TAG, null, TAG_NAME_CURRENCY)
        val typeString = parser.getAttributeValue(null, ATTRIBUTE_NAME_CURRENCY)
        val type = Currency.Type.fromValue(typeString)
        if (type != null) {
            val rate = parser.getAttributeValue(null, ATTRIBUTE_NAME_RATE).toDouble()
            result = Currency(type, rate)
        }
        parser.nextTag()
        parser.require(XmlPullParser.END_TAG, null, TAG_NAME_CURRENCY)
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

}
